package II;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Contact {
	private double balance;
	private Lock balanceChangedLock;
	private Condition enoughFundCondition;
	private double interestRate;
	private Queue queue;

	public Contact() {

		balanceChangedLock = new ReentrantLock();
		enoughFundCondition = balanceChangedLock.newCondition();
		queue = new Queue(10);
	}

	public void add(String amount) throws InterruptedException {
		try {
			balanceChangedLock.lock();

			while (queue.checkSize() > 10) {
				enoughFundCondition.await();
			}
			queue.add(amount);

			System.out.print("Add " + amount+"\n");
			System.out.println(queue.toString());
			enoughFundCondition.signalAll();
		} finally {
			balanceChangedLock.unlock();
		}
	}

	public void remove(String amount) throws InterruptedException {
		try {
			balanceChangedLock.lock();
			while (queue.checkSize() <= 0) {
				enoughFundCondition.await();
			}
			queue.remove(0);
			System.out.print("remove " + amount+"\n");
			System.out.println(queue.toString());
			enoughFundCondition.signalAll();
		} finally {
			balanceChangedLock.unlock();
		}

	}

}
