package II;

public class QueueThreadRunner {
	
	public static void main(String[] args) {
		Contact contact = new Contact();
		AddRunnable add = new AddRunnable(contact,100,"A");
		RemoveRunnable remove = new RemoveRunnable(contact,100,"A");
		
		Thread at = new Thread(add);
		Thread rt = new Thread(remove);
		
		at.start();
		rt.start();	

	}

}
