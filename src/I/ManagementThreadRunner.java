package I;


public class ManagementThreadRunner {
	
	public static void main(String[] args) {

		Management management = new Management();


		AddRunnable add = new AddRunnable(management, 3);
		RemoveRunnable remove = new RemoveRunnable(management, 2);

		
		Thread at = new Thread(add);
		Thread rt = new Thread(remove);
		
		at.start();
		rt.start();

	}

}
