package I;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Management {
	private Lock manageCountLock;
	private Condition manageCondition;
	private LinkedList<Integer> linkList;

	
	public Management() {
		manageCountLock = new ReentrantLock();
		manageCondition = manageCountLock.newCondition();
		linkList = new LinkedList<Integer>();
	}
	
	public void add() {
		manageCountLock.lock();
		try {
			linkList.add(1);
			System.out.println("Element added into LinkedList");
			manageCondition.signalAll();
		}
		finally {
			manageCountLock.unlock();
		}
	}
	
	public void remove() throws InterruptedException {
		manageCountLock.lock();
		try {
			while(linkList.size()==0){
				manageCondition.await();

			}
			linkList.removeLast();
			System.out.println("Element removed into LinkedList");
				
		}
		finally {
			manageCountLock.unlock();
		}
	}
	public int getElement(int element){
		return element;
	}

	
}
